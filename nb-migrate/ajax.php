<?php



// Our Makeshift Controller

if ( !function_exists( 'add_action' ) ) {

	$controller = new NB_Controller();

	$controller->dispatch();

	exit;

}



class NB_Controller

{

	var $MagentoApi;

	

	function __construct(){

		$this->MagentoApi = new NB_MagentoApi();

	}



	// Connects to store

	function action_connect(){

		//$this->MagentoApi->connect();

		$results = $this->MagentoApi->call('directory_country.list');

		

		pr($results);

	

		// If you don't need the session anymore

		//$client->endSession($this->session);

		

		//return $results;

	

	}

	

	function  action_list(){

		//if(!$this->getParam('sku')) return;

		$results = $this->MagentoApi->call('product.list');

		echo json_encode($results);

	}

	

	function  action_info(){

		if(!$this->getParam('sku')) return;

		$results = $this->MagentoApi->call('product.info',$this->getParam('sku'));

		echo json_encode($results);

	}

	function action_moveimage(){
		$data = array();
		$uploadsDirs = wp_upload_dir();
		$data['upload_dir'] = $uploadDirs;
		
		$baseDir = $uploadsDirs['basedir'] . '/import/' . $this->getParam('sku');
		$baseUrl = $uploadsDirs['baseurl'] . '/import/' . $this->getParam('sku');
		//if the directory doesn't exist, create it	
		if(!file_exists($baseDir)) {
			mkdir($baseDir);
		}
		
		$file_url = str_replace('~','/',$this->getParam('image_url'));
		$data['file_url'] = $file_url;
		$new_filename = array_pop(explode("/", $file_url));
		
		if (@fclose(@fopen($file_url, "r"))) { //make sure the file actually exists
			if(copy($file_url, $baseDir.'/'.$new_filename)){
				$data['file_location'] = $baseDir.'/'.$new_filename;
				$data['image_location'] = $baseUrl.'/'.$new_filename;
			} else {
				$data['error'] = 'copy failed';
			}
		} else {
			$data['error'] = 'open failed';
		}
		echo json_encode($data);
		
	}

	function action_migrate(){

		if(!$this->getParam('sku')){
			 echo json_encode(array('error'=>'must pass in a sku.'));
			 exit;
		}
		if(!$this->getParam('revised-post') || $this->getParam('revised-post') == ''){
			 echo json_encode(array('error'=>'must pass in a revised post content.'));
			 exit;
		}

		// amd_zlrecipe_select_db - Hopefully we can use there function to create the ZipList Recipe

		$results = $this->MagentoApi->call('product.info',$this->getParam('sku'));

		if(!isset($results['product_id'])) return;

		

		$new_post = array(

			'post_title' => $results['name'],

			'post_content' => $this->getParam('revised-post'),

			'post_status' => 'publish',

			'post_date' => date('Y-m-d H:i:s',strtotime($results['future_publish_date'])),

			'post_author' => 1,

			'post_type' => 'post',

			'post_category' => array(0)

		);

		

		$post_id = wp_insert_post($new_post);

		echo json_encode($post_id);

	

	}

	

	function dispatch(){

		if($_GET['action'] && method_exists($this,'action_' . $_GET['action'])){

			require_once('../../../wp-load.php');

			$this->{'action_' . $_GET['action']}();

		}	

	}

	

	function getParam($key){

		return (isset($_REQUEST[$key]))?$_REQUEST[$key]:false;

	}



}



class NB_MagentoApi

{

	var $session;

	var $client;

	

	function connect(){

		$this->client = new SoapClient(get_option('magentoStoreUrl'). 'api/?wsdl');

		$this->session = $this->client->login(get_option('magentoApiUser'), get_option('magentoApiPassword'));

		return $this->session;

	}

	

	function call($action, $data = null){

		if(!$this->session) $this->connect();

		try{

			$results = $this->client->call($this->session, $action, $data);

		} catch(Exception $e){

			$results = $e->getMessage();

		}

		return $results;

	}

	

}



?>

