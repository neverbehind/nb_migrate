<?php

/**

 * @package NB_Migrate

 * @version 0.1

 */

/*

Plugin Name: Never Behind Migration Tool

Plugin URI: http://www.neverbehind.com

Description: Plugin that assists in migration from Magento

Author: Geoff Douglas

Version: 0.1

Author URI: http://geoffreydouglas.com/

*/



// No Add Action

if ( !function_exists( 'add_action' ) ) {

	exit;

}



define( 'NB_MIGRATE__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );



// Add Menus

add_action('admin_menu', 'nb_migrate_menu_pages');



// Adds module to left sidebar in wp-admin for ZLRecipe

function nb_migrate_menu_pages() {

    // Add the top-level admin menu

    $page_title = 'Magento Migration Settings';

    $menu_title = 'Migration Settings';

    $capability = 'manage_options';

    $menu_slug = 'magemigration-settings';

    $function = 'nb_migration_settings';

    //add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function);
	add_options_page('Migration Settings', 'Magento Migration', 'manage_options', 'magento-settings', 'nb_migration_settings');


    // Add submenu page with same slug as parent to ensure no duplicates

    $settings_title = 'Settings';

    //add_submenu_page($menu_slug, $page_title, $settings_title, $capability, $menu_slug, $function);

	

	// Add Management Page for Migration

	add_management_page('Magento Migration', 'Magento', 'manage_options', 'magento-migration', 'nb_migration_management');

}



// Add 'Management' page to the NB Migration Module

function nb_migration_management(){

	nb_template_render('migration_management');

}



function nb_template_render($template,$vars = null){

	if($vars) extract($vars);

	ob_start();

	include ('templates/' . $template . '.phtml');

	echo ob_get_clean();

}



// Adds 'Settings' page to the NB Migration module

function nb_migration_settings() {

    if (!current_user_can('manage_options')) {

        wp_die('You do not have sufficient permissions to access this page.');

    }



	$settings = array(

		'magentoStoreUrl' => array(

			'title' => 'Magento Store Url',

			'type' => 'text',

			'placeholder'=>'http://www.example.com/'

		),

		'magentoApiUser' => array(

			'title' => 'Magento API User',

			'type' => 'text',

			'placeholder'=>'xxxxxxxxxx'

		),

		'magentoApiPassword' => array(

			'title' => 'Magento API Password',

			'type' => 'password',

			'placeholder'=>'xxxxxxxxxxxxxxx'

		),

	);



     if (isset($_POST['data']) && !empty($_POST['data'])) {

		foreach($_POST['data'] as $key => $value){

			update_option($key, $value);

		}

    }

	

	nb_template_render('settings_form',array('settings'=>$settings));



}



// Debug Helper

function pr($debug){

	printf('<pre>%s</pre>',print_r($debug,true));

}

?>